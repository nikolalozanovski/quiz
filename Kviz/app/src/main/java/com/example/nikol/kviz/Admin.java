package com.example.nikol.kviz;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by nikola on 22.3.17.
 */

public class Admin extends AppCompatActivity {
    private void ChangeIntent(String text) {
    if(text.equals("play"))
    {
        Intent i=new Intent(this,Game.class);
        startActivity(i);

    }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin);

        Button insert = (Button) findViewById(R.id.InsertButton);
        Button play = (Button) findViewById(R.id.PlayButton);
        play.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                ChangeIntent("play");
            }
        });
        insert.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {



               final Dialog d=new Dialog(v.getContext());
                d.setContentView(R.layout.my_dialog);
                d.setTitle("Insert new data");
                d.show();
                Button cancel = (Button) d.findViewById(R.id.cancelBtn);
                cancel.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View v)
                    {
                        d.hide();
                    }
                });
                Button ok = (Button) d.findViewById(R.id.okBtn);



                ok.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                         EditText club = (EditText) d.findViewById(R.id.Club);
                         EditText city = (EditText) d.findViewById(R.id.City);

                        if (city.getText().toString().equals("") || club.getText().toString().equals("")) {
                            Toast.makeText(v.getContext(), "Failed to write new pair, missing text", Toast.LENGTH_SHORT);
                        } else {
                            Folder fold = new Folder("/sdcard/Quiz");
                            FileSource fs = new FileSource(fold, "/game.txt");
                            fs.Write(club.getText().toString(),city.getText().toString() );
                            Toast.makeText(v.getContext(),"Successfully inserted club and city",Toast.LENGTH_SHORT);
                            d.hide();
                        }
                    }
                });
            }
        });
    }
}
