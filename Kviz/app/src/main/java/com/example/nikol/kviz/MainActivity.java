package com.example.nikol.kviz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;


public class MainActivity extends AppCompatActivity {
private FileSource file;

    private void ChangeIntent()
    {
        EditText user=(EditText)findViewById(R.id.Username);
        EditText pass=(EditText) findViewById(R.id.Password);
        String username=user.getText().toString();
        String password=pass.getText().toString();
        Boolean isAdmin= file.FindPair(username,password);
        Intent intent;


        if(username.equals("")&&password.equals(""))
        {
            intent = new Intent(this, Game.class);

        }
        else if(isAdmin==true)
        {
            intent =new Intent(this,Admin.class);
        }
        else
        {
            intent=new Intent(this,MainActivity.class);
            Toast.makeText(this,"Failed to start game",Toast.LENGTH_SHORT);
        }
        startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {


        //if you want to insert new club/city at start you should do this:

        //create folder or open existing
        //Folder object = new Folder("/sdcard/*name of the folder* ");

        //create file
        // FileSource object = new FileSource(*object of the folder*,"*file name*");

        //use file method named Write()
        //object.Write("City","Club");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Folder fold=new Folder("/sdcard/Quiz");
        FileSource fs=new FileSource(fold,"/game.txt");
        FileSource admin=new FileSource(fold,"/admin.txt");
        admin.Write("Nikola","Lozanovski");
        fs.Read();
        file=admin;
        final Button button = (Button) findViewById(R.id.login);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                ChangeIntent();
            }
        });


    }


}
