package com.example.nikol.kviz;


import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

/**
 * Class used to store data from text file
 */
class elements_pair {
    public String first;//city
    public String second;//club
    public boolean isChosen;
    public elements_pair(String First, String Second) {
        first = First;
        second = Second;
    }
}
class Folder
{
    public String name;
    File Location;
    public Folder(String Name)
    {
            name = Name;
            Location = new File(Name);
            if (!Location.exists()) {

                if (!Location.mkdirs()) {
                    Log.d("Filesystem", "<" + Name + "> folder failed to create!");
                } else {
                    Log.d("Filesystem", "Folder created with name: " + Name);
                }
            } else {
                Log.d("Filesystem", "Folder <" + Name + "> already exists!");
            }
    }
}
/**
 * Class used to open file, read and write to
 */
public class FileSource {
    private boolean isOpen = false;
    private File source;
    public static Vector<elements_pair> pair = new Vector<>();

    public FileSource(Folder folder ,String Source)
    {
        Openfile(folder,Source);
    }
    public FileSource()
    {

    }
    public void Openfile(Folder folder,String Source) {
        source = new File(folder.Location,Source);
        if(!source.exists()) {
            try {
                FileWriter writer = new FileWriter(source, true);
                Log.d("Filesystem","File "+folder.name+Source+" is created");
            } catch (IOException e) {
                Log.d("Error", e.toString());
            }
        }else
        {
            Log.d("Filesystem","File "+folder.name+Source+" already exist and is opened");
        }
    }

    public void Read() {
        if(!isOpen) {
            isOpen = true;
//            StringBuilder text = new StringBuilder();
            try {
                BufferedReader br = new BufferedReader(new FileReader(source));
                String line;

                while ((line = br.readLine()) != null) {
                    String elements[];
                    elements = line.split(",", 2);
                    String city;
                    String club;
                    city = elements[1];
                    club = elements[0];
                    elements_pair pair_elem = new elements_pair(city, club);
                    pair.add(pair_elem);
                    isOpen=false;

                }
                br.close();
            } catch (IOException e) {
                Log.d("Filesystem", e.getMessage());
            }
        }
        else
        {
            Log.d("Fileopen","Cannot open this file");
        }

    }
    public   boolean FindPair(String First, String Second)
    {
        try {
            BufferedReader br = new BufferedReader(new FileReader(source));
            String line;

            while ((line = br.readLine()) != null) {
                String elements[];
                String first;
                String second;
                elements = line.split(",", 2);
                first = elements[0];
                second = elements[1];
                //Log.d("check",club+" "+Club);
                if(Second.equals(second)) {
                    Log.d("equals",second+" "+Second);
                    return true;
                }
            }
            Log.d("find pair","END");
            br.close();
        } catch (IOException e) {
            Log.d("Filesystem", e.getMessage());
        }
        return false;
    }
    public void Write(String City, String Club) {
        if(!isOpen) {
        try {

            isOpen=true;
            BufferedWriter writer = new BufferedWriter(new FileWriter(source,true));
            if(!FindPair(City,Club)) {
                writer.append(City + "," + Club+System.getProperty("line.separator"));
                Log.d("WRITE"," Club: "+Club+" with city: "+City+" are written in file");
            }
            writer.close();
            isOpen=false;

        }catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.getMessage());
         }
        }
        else
        {
            Log.d("Fileopen","Cannot open this file");
        }
    }

    public void PrintData()
    {

        for (elements_pair o : pair) {

            Log.d("READ",String.valueOf(o.first+" "+o.second));

        }
    }
}
