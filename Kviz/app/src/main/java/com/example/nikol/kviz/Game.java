package com.example.nikol.kviz;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.style.UpdateLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Vector;

/**
 * Created by nikola on 7.3.17.
 */
class ClubAdapter extends BaseAdapter
{

    private Context context;

    int correctID=-1;
    int score=0;
    private FileSource fs=new FileSource();
    public ClubAdapter(Context context)
    {
        this.context=context;
    }
    private static LayoutInflater inflater=null;
    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Object getItem(int position) {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
Vector<elements_pair>tem=new Vector<>();
    private elements_pair getResources()
    {
        int begin=0, end=fs.pair.size();
        int pos=0;


            Random r = new Random();
            pos=r.nextInt(end-begin)+begin;
        elements_pair temp=fs.pair.elementAt(pos);

        tem.add(fs.pair.remove(pos));
        return temp;
    }
    private ArrayList<Integer> randomNumbers=new ArrayList<>();
    private int getRandomNumber()
    {

        int low = 0, high = randomNumbers.size(), pos;
        Random r = new Random();
        pos = r.nextInt(high - low) + low;
        int number=randomNumbers.get(pos);

        randomNumbers.remove(pos);
        return number;
    }
    private void checkWin(int number, View v)
    {
        Log.d("check ",String.valueOf(number)+" "+String.valueOf(correctID));
        if(number==correctID)
        {
            score++;
            Toast.makeText(v.getContext(),"Correct!",Toast.LENGTH_SHORT).show();

        }
        else
        {
            Toast.makeText(v.getContext(),"Incorrect!",Toast.LENGTH_SHORT).show();
            if(score>0)
            {
                score--;
            }

        }
//        Log.d("rezultat",String.valueOf(g.score));
    }
    private void setResources(TextView txt,Button one,Button two,Button three,Button four,TextView Score)
    {
        String club="", city1="", city2="", city3="", city4="";

        for(int i=1;i<5;i++)
        {
            randomNumbers.add(i);
        }
        Collections.shuffle(randomNumbers);

        int prv=getRandomNumber();
        int vtor=getRandomNumber();
        int tret=getRandomNumber();
        int cet=getRandomNumber();



        elements_pair eden=getResources();
        elements_pair dva=getResources();
        elements_pair tri=getResources();
        elements_pair cetiri=getResources();


//        club=eden.club;
//        city1=eden.city;
//        city2=dva.city;
//        city3=tri.city;
//        city4=cetiri.city;

        //club=eden.club;
        if(prv==1)
        {
            city1=eden.first;
            correctID=1;
        }
        else if (prv==2)
        {
            city2=dva.first;
        }
        else if(prv==3)
        {
            city3=tri.first;
        }
        else if(prv==4)
        {
            city4=cetiri.first;
        }

        if(vtor==1)
        {
            city1=eden.first;
            correctID=1;
        }
        else if (vtor==2)
        {
            city2=dva.first;
        }
        else if(vtor==3)
        {
            city3=tri.first;
        }
        else if(vtor==4)
        {
            city4=cetiri.first;
        }

        if(tret==1)
        {
            city1=eden.first;
            correctID=1;
        }
        else if (tret==2)
        {
            city2=dva.first;
        }
        else if(tret==3)
        {
            city3=tri.first;
        }
        else if(tret==4)
        {
            city4=cetiri.first;
        }

        if(cet==1)
        {
            city1=eden.first;
            correctID=1;
        }
        else if (cet==2)
        {
            city2=dva.first;
        }
        else if(cet==3)
        {
            city3=tri.first;
        }
        else if(cet==4)
        {
            city4=cetiri.first;
        }
        Random r = new Random();
        correctID=r.nextInt(4-1)+1;
        if(correctID==1)
        {
            club=eden.second;
        }
        else if(correctID==2)
        {
            club=dva.second;
        }
        else if(correctID==3)
        {
            club=tri.second;
        }
        else if(correctID==4)
        {
            club=cetiri.second;
        }
        else
        {

        }

        txt.setText(club);
        one.setText(city1);
        two.setText(city2);
        three.setText(city3);
        four.setText(city4);
        Score.setText("Score is: "+String.valueOf(score));

    }
    boolean controlLoop=false;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        if(convertView==null) {
            inflater = LayoutInflater.from(this.context);
            convertView = inflater.inflate(R.layout.single_element, parent,false);



            TextView txtClub = (TextView) convertView.findViewById(R.id.txtClub);
            Button oneCity = (Button) convertView.findViewById(R.id.clubPlace1);
            Button twoCity = (Button) convertView.findViewById(R.id.clubPlace2);
            Button threeCity = (Button) convertView.findViewById(R.id.clubPlace3);
            Button fourCity = (Button) convertView.findViewById(R.id.clubPlace4);
            TextView myScore=(TextView)convertView.findViewById(R.id.myScore);
            if(!controlLoop) {
                controlLoop=true;
                setResources(txtClub, oneCity, twoCity, threeCity, fourCity,myScore);
            }

//
            oneCity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    controlLoop=false;
                    Button btn=(Button)v;
                    String text=btn.getText().toString();
                    checkWin(1,v);

                    View vv=(View)v.getParent();

                    TextView txtClub = (TextView) vv.findViewById(R.id.txtClub);
                    Button oneCity = (Button) vv.findViewById(R.id.clubPlace1);
                    Button twoCity = (Button) vv.findViewById(R.id.clubPlace2);
                    Button threeCity = (Button) vv.findViewById(R.id.clubPlace3);
                    Button fourCity = (Button) vv.findViewById(R.id.clubPlace4);
                    TextView myScore=(TextView)vv.findViewById(R.id.myScore);

                    setResources(txtClub, oneCity, twoCity, threeCity, fourCity,myScore);
                    notifyDataSetChanged();
                }
            });
            twoCity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    controlLoop=false;
                    Button btn=(Button)v;
                    String text=btn.getText().toString();
                    checkWin(2,v);

                    View vv=(View)v.getParent();

                    TextView txtClub = (TextView) vv.findViewById(R.id.txtClub);
                    Button oneCity = (Button) vv.findViewById(R.id.clubPlace1);
                    Button twoCity = (Button) vv.findViewById(R.id.clubPlace2);
                    Button threeCity = (Button) vv.findViewById(R.id.clubPlace3);
                    Button fourCity = (Button) vv.findViewById(R.id.clubPlace4);
                    TextView myScore=(TextView)vv.findViewById(R.id.myScore);

                    setResources(txtClub, oneCity, twoCity, threeCity, fourCity,myScore);
                    notifyDataSetChanged();

                }
            });
            fourCity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    controlLoop=false;
                    Button btn=(Button)v;
                    String text=btn.getText().toString();
                   checkWin(4,v);
                    View vv=(View)v.getParent();

                    TextView txtClub = (TextView) vv.findViewById(R.id.txtClub);
                    Button oneCity = (Button) vv.findViewById(R.id.clubPlace1);
                    Button twoCity = (Button) vv.findViewById(R.id.clubPlace2);
                    Button threeCity = (Button) vv.findViewById(R.id.clubPlace3);
                    Button fourCity = (Button) vv.findViewById(R.id.clubPlace4);
                    TextView myScore=(TextView)vv.findViewById(R.id.myScore);

                    setResources(txtClub, oneCity, twoCity, threeCity, fourCity,myScore);
                    notifyDataSetChanged();

                }
            });
            threeCity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    controlLoop=false;
                    Button btn=(Button)v;
                    String text=btn.getText().toString();
                   checkWin(3,v);
                    View vv=(View)v.getParent();

                    TextView txtClub = (TextView) vv.findViewById(R.id.txtClub);
                    Button oneCity = (Button) vv.findViewById(R.id.clubPlace1);
                    Button twoCity = (Button) vv.findViewById(R.id.clubPlace2);
                    Button threeCity = (Button) vv.findViewById(R.id.clubPlace3);
                    Button fourCity = (Button) vv.findViewById(R.id.clubPlace4);
                    TextView myScore=(TextView)vv.findViewById(R.id.myScore);

                    setResources(txtClub, oneCity, twoCity, threeCity, fourCity,myScore);
                    notifyDataSetChanged();

                }
            });

        }
        return convertView;
    }
}

public class Game extends AppCompatActivity {

    ClubAdapter CA=new ClubAdapter(this);

public static int score=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.game);
        ListView li;
        li=(ListView)findViewById(R.id.clubsListView);
        li.setAdapter(CA);
    };


}
